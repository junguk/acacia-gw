import subprocess
import re

def get_mac(ip):
    domain_name = subprocess.check_output(["hostname | sed s/`hostname -s`.//"], shell=True).rstrip()
    ar_node = "ar.%s" % domain_name
    comm = "ifconfig | grep -B1 %s" % ip
    ssh_p = subprocess.Popen(["ssh", ar_node, comm], stdout=subprocess.PIPE)
    cloud_mac = re.search(r'HWaddr (.*)',ssh_p.communicate()[0]).group(1).split()[0]
    return cloud_mac
