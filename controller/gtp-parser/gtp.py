
# Copyright (C) 2012 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import struct

from . import packet_base
from . import packet_utils
from ryu.ofproto import ether


class gtp(packet_base.PacketBase):
    _PACK_STR = '!BBHI'
    _MIN_LEN = struct.calcsize(_PACK_STR)

    def __init__(self, flag=48, msg_type=255, total_length=0, teid=0):
        super(gtp, self).__init__()
	self.flag = flag
        self.msg_type = msg_type
	self.total_length = total_length
        self.teid = teid

    @classmethod
    def parser(cls, buf):
        (flag, msg_type, length, teid) = struct.unpack_from(gtp._PACK_STR, buf)
        msg = cls(flag, msg_type, length, teid) 
        return msg, gtp.get_packet_type(ether.ETH_TYPE_IP), buf[msg._MIN_LEN:length]

    def serialize(self, payload, prev):
        if self.total_length == 0:
            self.total_length = udp._MIN_LEN + len(payload)
        h = struct.pack(gtp._PACK_STR, self.flag, self.msg_type, self.total_length, self.teid)
        return h
