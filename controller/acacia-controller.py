# Copyright (C) 2011 Nippon Telegraph and Telephone Corporation.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet

# with new ryu
#from ryu.lib.packet import ether_types,in_proto

# with old ryu - will use ether_types instead ether and in_proto instead inet
from ryu.ofproto import ether,inet


# FOR REST API
import json
from webob import Response
from ryu.controller import dpset
from ryu.app.wsgi import ControllerBase, WSGIApplication
from ryu.ofproto import nx_match
import re
import socket
from ryu.ofproto import ether

import json
from enb import *
import sys

'''
   TODO 
   1. Do not get mac address of enb and cloud -> patch OVS to support them
'''

NET_OF_SGWU="192.168.20.221"
NET_OF_PGWU="192.168.20.211"
GWUS_DB = "gwu.json"
ENB_DB = "enb.json"
SGWU="sgwu"
PGWU="pgwu"
ENB="enb"
CLOUD="cloud"
UL="ul"
DL="dl"

''' 
	SGW , input_port => ned_d, output_port => net_b
	PGW , input_port => net_b, output_port => off
'''
class GWU:
    INPUT_PORT=1
    OUTPUT_PORT=2
    GTP=3
    DECAP=4
    ENCAP=5
    GTP_PORT=2152

    gwu_name = None
    input_port_ip = None
    output_port_ip = None
    input_port_mac = None
    output_port_mac = None
    datapath = None

    # SGW -> enb's ip and mac
	# PGW -> cloud's ip and mac
    external_port_ip = None
    external_port_mac = None

    def __init__(self, gwu_name):
        self.gwu_name = gwu_name

    def set_external_port_info(self, port_ip, port_mac):
        self.external_port_ip = port_ip
        self.external_port_mac = port_mac

    def set_sgwu_ips(self, gwu_ips):
        self.input_port_ip = gwu_ips["sgwu_s11"]
        self.output_port_ip = gwu_ips["sgwu_s5"]
        NET_OF_SGWU = gwu_ips["net_of"]

    def set_pgwu_ips(self, gwu_ips):
        self.input_port_ip = gwu_ips["pgwu_s5"]
        self.output_port_ip = None
        NET_OF_PGWU = gwu_ips["net_of"]

    def set_switch_dp(self, datapath):
        self.datapath = datapath

    def set_ports_mac(self, input_port_mac, output_port_mac):
        self.input_port_mac = input_port_mac
        self.output_port_mac = output_port_mac

    def print_gw_info(self):
        print "name = %s datapath = %s input port = %s %s , output port = %s %s external port = %s %s" % (self.gwu_name, self.datapath.id, self.input_port_ip, self.input_port_mac,self.output_port_ip, self.output_port_mac, self.external_port_ip, self.external_port_mac)

# Make connection keep-alive
class flow_manager(ControllerBase):
    def __init__(self, req, link, data, **config):
        super(flow_manager, self).__init__(req, link, data, **config)
        #print "flow_manager INIT"
        self.dpset = data['dpset']
        self.waiters = data['waiters']
        self.controller = data['controller']

    # req -> HTTP request
    # **_kwargs
    def get_flow(self, req, **_kwargs):
       	print "provide flow info "
        print req.body
        print _kwargs
        body = json.dumps("Insert flow rules")
        return (Response(content_type='application/json', body=body))

    def set_flow(self, req, **_kwargs):
        flowInfo = json.loads(req.body)
        #print req.body
        #print type(req.body)
        #print flowInfo

	    # TODO : will be a thread
        self.controller.generate_flow_rules(flowInfo)
        return Response(status=200)


class SimpleSwitch14(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    _CONTEXTS = {
        'dpset': dpset.DPSet,
        'wsgi': WSGIApplication
    }

    def dump_all_switches(self):
        print "dump_all_switches"
        for (key, value) in self.dpset.dps.items():
            print self.dpset.get_ports(key)
            for ele in self.dpset.get_ports(key):
                print "port name = %s port no = %d" % (ele.name,ele.port_no)
                
    def dump_switch(self,dpID):
        print "dump switch info"
        print self.dpset.get_ports(dpID)

    def __init__(self, *args, **kwargs):
        super(SimpleSwitch14, self).__init__(*args, **kwargs)
		# To manage multiple switches 
        self.hostname_to_gwu = {}
        self.dp_to_gwu = {}
        self.enb_info = None
        self.gwu_count = 0

        self.init_rest_api(**kwargs)
        self.init_enb_info()
        self.init_gwu_info()
    
    def init_rest_api(self,**kwargs):
        #### INITIALIZE REST ####
        self.dpset = kwargs['dpset']
        wsgi = kwargs['wsgi']
        self.waiters = {}
        self.data = {}
        self.data['dpset'] = self.dpset
        self.data['waiters'] = self.waiters
        self.data['controller'] = self
        mapper = wsgi.mapper

        wsgi.registory['flow_manager'] = self.data
        #wsgi.registory['Stat'] = self
        uri = '/flow_manager/get_flow'
        mapper.connect('flow_manager', uri,
                       controller=flow_manager, action='get_flow',
                       conditions=dict(method=['GET']))

        uri = '/flow_manager/set_flow'
        mapper.connect('flow_manager', uri,
                       controller=flow_manager, action='set_flow',
                       conditions=dict(method=['POST']))


    def init_enb_info(self):
        self.enb_info = eNBs(ENB_DB)

    def init_gwu_info(self):
        with open('gwu.json') as data_file:
            data = json.load(data_file)
            sgwu = GWU(SGWU)
            sgwu_ips = data[SGWU]
            sgwu.set_sgwu_ips(sgwu_ips)
            enb_name = data[ENB]
            enb = self.enb_info.get_enb_info(enb_name)
            sgwu.set_external_port_info(enb[0], enb[1])
            self.hostname_to_gwu[SGWU] = sgwu

            pgwu = GWU(PGWU)
            pgwu_ips = data[PGWU]
            pgwu.set_pgwu_ips(pgwu_ips)


            from utils import *
            external_ip = data[CLOUD]
            external_mac = get_mac(external_ip)
            print "CLOUD IP = %s" % external_ip
            print "CLOUD MAC = %s" % external_mac
            pgwu.set_external_port_info(external_ip, external_mac)
            self.hostname_to_gwu[PGWU] = pgwu

    # PORT INFORMATION  - http://ryu.readthedocs.org/en/latest/ofproto_v1_3_ref.html
    def send_port_desc_stats_request(self, datapath):
        ofp_parser = datapath.ofproto_parser
 
        req = ofp_parser.OFPPortDescStatsRequest(datapath, 0)
        datapath.send_msg(req)
 
    # PORT INFORMATION  - http://ryu.readthedocs.org/en/latest/ofproto_v1_3_ref.html
    @set_ev_cls(ofp_event.EventOFPPortDescStatsReply, MAIN_DISPATCHER)
    def port_desc_stats_reply_handler(self, ev):
        ports = []
        #print type(ev)
        #print ev
        #print ev.msg
        datapath = ev.msg.datapath
        gwu = self.dp_to_gwu[datapath.id]
        input_port_mac = None
        output_port_mac = None

        for p in ev.msg.body:
            if p.port_no == GWU.INPUT_PORT:
                input_port_mac = p.hw_addr
            elif p.port_no == GWU.OUTPUT_PORT:
                output_port_mac = p.hw_addr
            else:
                pass

        if (input_port_mac == None) or (output_port_mac == None):
            print "Cannot get ports mac\n"

        gwu.set_ports_mac(input_port_mac, output_port_mac)
        gwu.print_gw_info()

        pgwu = self.hostname_to_gwu[PGWU]
        sgwu = self.hostname_to_gwu[SGWU]

        self.gwu_count = self.gwu_count + 1

        if self.gwu_count == 2:
            print "Both SGWU & PGWU are ready, install default flows\n"
            self.set_sgwu_default_flow(sgwu.datapath, sgwu, pgwu)
            self.set_pgwu_default_flow(pgwu.datapath, sgwu, pgwu)
        else:
            print "Both SGWU & PGWU are not ready yet\n"

    # class Datapath class is in ryu/controller/controller.py
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        address = datapath.address

        self.delete_all_flow(datapath)

        #Get ports information from connected switch
        ip = address[0] # str
        port = address[1]
        if ip == NET_OF_SGWU:
            gwu_name=SGWU
            print "SGWU is connected"
        else:
            gwu_name=PGWU
            print "PGWU is connected"

        gwu = self.hostname_to_gwu[gwu_name]
        gwu.set_switch_dp(datapath)
        self.dp_to_gwu[datapath.id] = gwu

		# To get port information (e.g., MAC address)
        self.send_port_desc_stats_request(datapath)
        print "\n\n############# Switch %s is connected #############" % datapath.id
        #self.dump_all_switches()

    def delete_flow_mod(self, datapath, match, out_port, priority=1):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        mod = parser.OFPFlowMod(datapath=datapath, command=ofproto.OFPFC_DELETE, 
				priority=priority, out_port=out_port, out_group=ofproto.OFPG_ANY,match=match)
        datapath.send_msg(mod)


    def delete_all_flow(self, datapath):
        print "\n\n############ Delete all flow rules ###############"
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        match = parser.OFPMatch()
        self.delete_flow_mod(datapath, match, ofproto.OFPP_ANY)

	''' set flow rule '''
    def set_flow_mod(self, datapath, match, actions, priority=1, idle_timeout=0):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,actions)]
        mod = parser.OFPFlowMod(datapath=datapath, idle_timeout=idle_timeout, command=ofproto.OFPFC_ADD, priority=priority,match=match, instructions=inst) # , flags=ofproto.OFPFF_SEND_FLOW_REM)
        datapath.send_msg(mod)

    # HANDLE ARP REQUEST BTW ENB, GWS
    def set_arp_flow(self,datapath,port,mac):
        priority = 3
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser	
        
        match = parser.OFPMatch(in_port=port,eth_type=ether.ETH_TYPE_ARP)
        actions = []
        actions.append(parser.OFPActionOutput(ofproto.OFPP_LOCAL))
        self.set_flow_mod(datapath, match, actions, priority)

        match = parser.OFPMatch(in_port=ofproto.OFPP_LOCAL,eth_type=ether.ETH_TYPE_ARP, eth_dst=mac) 
        actions = []
        actions.append(parser.OFPActionOutput(port))
        self.set_flow_mod(datapath, match, actions, priority)

    def set_gwu_default_arp_flow(self,datapath,ul_port, ul_mac,dl_port,dl_mac):
        self.set_arp_flow(datapath, ul_port, ul_mac)
        self.set_arp_flow(datapath, dl_port, dl_mac)

    def set_sgwu_default_flow(self,datapath, sgwu, pgwu):
        self.set_gwu_default_arp_flow(datapath, sgwu.INPUT_PORT, sgwu.external_port_mac, sgwu.OUTPUT_PORT, pgwu.input_port_mac)
        self.set_sgwu_default_tunnel_flow(datapath, sgwu)

    def set_pgwu_default_flow(self,datapath, sgwu, pgwu):
        self.set_gwu_default_arp_flow(datapath, pgwu.INPUT_PORT, sgwu.output_port_mac, pgwu.OUTPUT_PORT, pgwu.external_port_mac)
        self.set_pgwu_default_tunnel_flow(datapath, pgwu)

    def set_sgwu_default_tunnel_flow(self,datapath,sgwu):
        priority = 3
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

	    # UPLINK from enb 
        # packet/in_proto.py
        match = parser.OFPMatch(in_port=sgwu.INPUT_PORT,eth_type=ether.ETH_TYPE_IP,ip_proto=inet.IPPROTO_UDP,udp_dst=sgwu.GTP_PORT)
        actions = []
        actions.append(parser.OFPActionOutput(sgwu.GTP))
        self.set_flow_mod(datapath, match, actions, priority)

        match = parser.OFPMatch(in_port=sgwu.ENCAP,eth_type=ether.ETH_TYPE_IP,ipv4_src=sgwu.output_port_ip)
        actions = []
        actions.append(parser.OFPActionOutput(sgwu.OUTPUT_PORT))
        self.set_flow_mod(datapath, match, actions, priority)

        # DOWNLINK from pgwu
        match = parser.OFPMatch(in_port=sgwu.OUTPUT_PORT,eth_type=ether.ETH_TYPE_IP,ip_proto=inet.IPPROTO_UDP,udp_dst=sgwu.GTP_PORT)
        actions = []
        actions.append(parser.OFPActionOutput(sgwu.GTP))
        self.set_flow_mod(datapath, match, actions, priority)

        # Does it need ipv4_src ??
        match = parser.OFPMatch(in_port=sgwu.ENCAP,eth_type=ether.ETH_TYPE_IP,ipv4_src=sgwu.input_port_ip)
        actions = []
        actions.append(parser.OFPActionOutput(sgwu.INPUT_PORT))
        self.set_flow_mod(datapath, match, actions, priority)

    def set_pgwu_default_tunnel_flow(self,datapath,pgwu):
        priority = 3
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
                                                                                                                           
        # UPLINK from sgwu
        # packet/in_proto.py
        match = parser.OFPMatch(in_port=pgwu.INPUT_PORT,eth_type=ether.ETH_TYPE_IP,ip_proto=inet.IPPROTO_UDP,udp_dst=pgwu.GTP_PORT)
        actions = []
        actions.append(parser.OFPActionOutput(pgwu.GTP))
        self.set_flow_mod(datapath, match, actions, priority)

        # DOWNLINK from cloud
        match = parser.OFPMatch(in_port=pgwu.ENCAP,eth_type=ether.ETH_TYPE_IP)
        actions = []
        actions.append(parser.OFPActionOutput(pgwu.INPUT_PORT))
        self.set_flow_mod(datapath, match, actions, priority)

    def generate_flow_rules(self,flowInfo):
        gwu_name = flowInfo[0]
        direction = flowInfo[1]
        user_ip = socket.ntohl(flowInfo[2])

        pgwu = self.hostname_to_gwu[PGWU]
        sgwu = self.hostname_to_gwu[SGWU]
        if gwu_name == PGWU:
            if direction == UL: # TODO : change UL
                print "SET PGWU UPLINK FLOW (SGWU->PGWU)"
                print "user ip : %s PGW T : %s (%s)" % (user_ip, flowInfo[3], hex(int(flowInfo[3])))
                self.set_pgwu_tunnel_ul_flow(pgwu.datapath, user_ip, flowInfo[3],pgwu)
            elif direction == DL:
                print "SET PGWU TUNNEL DOWNLINK FLOW (SGWU<-PGWU)"
                print "user ip : %s SGW T : %s (%s)" % (user_ip, flowInfo[3], hex(int(flowInfo[3])))
                self.set_pgwu_tunnel_dl_flow(pgwu.datapath, user_ip, flowInfo[3],pgwu,sgwu)
            else:
                print "something wrong"
                pass
        else: # "sgw"
            if direction == UL: # TODO : change UL
                print "SET SGWU UPLINK FLOW (SGWU<-ENB)"
                print "user ip : %s SGW T : %s (%s) PGW T : %s (%s)" % (user_ip, flowInfo[3], hex(int(flowInfo[3])), flowInfo[4], hex(int(flowInfo[4])))
                self.set_sgwu_tunnel_ul_flow(sgwu.datapath,user_ip, flowInfo[3], flowInfo[4], sgwu, pgwu)
            elif direction == DL:
                print "SET SGWU DOWNLINK FLOW (SGWU->ENB)"
                print "user ip : %s ENB T : %s (%s) SGW T : %s (%s)" % (user_ip, flowInfo[3], hex(int(flowInfo[3])), flowInfo[4], hex(int(flowInfo[4])))
                self.set_sgwu_tunnel_dl_flow(sgwu.datapath, user_ip, flowInfo[3], flowInfo[4],sgwu)
            else:
                print "something wrong"
                pass

    def set_sgwu_tunnel_dl_flow(self,datapath, user_ip, enodeb_teid, sgw_teid, sgwu):
        priority = 3
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

	# TODO : Does it need to check sgw_teid ?? It causes traffic forwarding when service request happens!

 	# for old version ryu
        match = parser.OFPMatch(in_port=sgwu.GTP,tunnel_id=sgw_teid,tun_src=user_ip)
    	# for recent version ryu
        #match = parser.OFPMatch(in_port=GTP,tunnel_id_nxm=sgw_teid,tun_ipv4_src=user_ip)
        actions = []
        actions.append(parser.OFPActionOutput(sgwu.DECAP))
        self.set_flow_mod(datapath, match, actions, priority)

        match = parser.OFPMatch(in_port=sgwu.DECAP,eth_type=ether.ETH_TYPE_IP,ipv4_dst=user_ip)
        #match = parser.OFPMatch(in_port=sgwu.DECAP,eth_type=ether.ETH_TYPE_IP,ipv4_dst=sgwu.output_port_ip)
        actions = []
        actions.append(parser.OFPActionSetField(eth_dst=sgwu.external_port_mac))
        actions.append(parser.OFPActionSetField(eth_src=sgwu.input_port_mac))
	    # For for old version ryu
        actions.append(parser.OFPActionSetField(tunnel_id=enodeb_teid))
        actions.append(parser.OFPActionSetField(tun_dst=sgwu.external_port_ip))
        actions.append(parser.OFPActionSetField(tun_src=sgwu.input_port_ip))

	# For for recent version ryu
#        actions.append(parser.OFPActionSetField(tunnel_id_nxm=enodeb_teid))
#        actions.append(parser.OFPActionSetField(tun_ipv4_dst=ENODEB_IP))
#        actions.append(parser.OFPActionSetField(tun_ipv4_src=NET_D_SGW_IP))
        actions.append(parser.OFPActionOutput(sgwu.ENCAP))
        self.set_flow_mod(datapath, match, actions, priority)

    def set_sgwu_tunnel_ul_flow(self,datapath,user_ip,net_b_sgw_teid,pgw_teid, sgwu, pgwu):
	priority = 3
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        match = parser.OFPMatch(in_port=sgwu.DECAP,eth_type=ether.ETH_TYPE_IP,ipv4_src=user_ip)
        #match = parser.OFPMatch(in_port=sgwu.DECAP,eth_type=ether.ETH_TYPE_IP,ipv4_src=sgwu.external_port_ip)
        actions = []
        actions.append(parser.OFPActionSetField(eth_src=sgwu.output_port_mac))
        actions.append(parser.OFPActionSetField(eth_dst=pgwu.input_port_mac)) 
	# for old version ryu
        actions.append(parser.OFPActionSetField(tunnel_id=pgw_teid))
        actions.append(parser.OFPActionSetField(tun_src=sgwu.output_port_ip))
        actions.append(parser.OFPActionSetField(tun_dst=pgwu.input_port_ip))

	# for recent version ryu - ofproto/nx_match.py
        #actions.append(parser.OFPActionSetField(tunnel_id_nxm=pgw_teid))
        #actions.append(parser.OFPActionSetField(tun_ipv4_dst=NET_B_PGW_IP))
        #actions.append(parser.OFPActionSetField(tun_ipv4_src=NET_B_SGW_IP))
        actions.append(parser.OFPActionOutput(sgwu.ENCAP))
        self.set_flow_mod(datapath, match, actions, priority)

	# for old version ryu
        match = parser.OFPMatch(in_port=sgwu.GTP,tunnel_id=net_b_sgw_teid,tun_dst=user_ip)
	# for recent version ryu
        #match = parser.OFPMatch(in_port=GTP,tunnel_id_nxm=net_b_sgw_teid,tun_ipv4_dst=user_ip)
        actions = []
        actions.append(parser.OFPActionOutput(sgwu.DECAP))
        self.set_flow_mod(datapath, match, actions, priority)

    # print "SET PGWU UPLINK FLOW (SGWU->PGWU)"
    def set_pgwu_tunnel_ul_flow(self, datapath, user_ip, pgw_teid, pgwu):
        priority = 3
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

	    # for old version ryu
        match = parser.OFPMatch(in_port=pgwu.GTP,tunnel_id=pgw_teid,tun_src=user_ip)
	    # for recent version ryu - ofproto/nx_match.py
        #match = parser.OFPMatch(in_port=GTP,tunnel_id_nxm=pgw_teid,tun_ipv4_src=user_ip)
        actions = []
        actions.append(parser.OFPActionSetField(eth_dst=pgwu.external_port_mac)) 
        actions.append(parser.OFPActionSetField(eth_src=pgwu.output_port_mac))
        actions.append(parser.OFPActionOutput(pgwu.DECAP))
        self.set_flow_mod(datapath, match, actions, priority)

        match = parser.OFPMatch(in_port=pgwu.DECAP,eth_type=ether.ETH_TYPE_IP,ipv4_src=user_ip)
        actions = []
        actions.append(parser.OFPActionOutput(pgwu.OUTPUT_PORT))
        self.set_flow_mod(datapath, match, actions, priority)

    # "SET PGWU TUNNEL DOWNLINK FLOW (SGWU<-PGWU)"
    def set_pgwu_tunnel_dl_flow(self,datapath,user_ip,net_b_sgw_teid, pgwu, sgwu):
        priority = 3
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        match = parser.OFPMatch(in_port=pgwu.OUTPUT_PORT, eth_type=ether.ETH_TYPE_IP,ipv4_dst=user_ip)
        actions = []
        actions.append(parser.OFPActionSetField(eth_dst=sgwu.output_port_mac)) 
        actions.append(parser.OFPActionSetField(eth_src=pgwu.input_port_mac))

	# for old version ryu
        actions.append(parser.OFPActionSetField(tunnel_id=net_b_sgw_teid))
        actions.append(parser.OFPActionSetField(tun_dst=sgwu.output_port_ip))
        actions.append(parser.OFPActionSetField(tun_src=pgwu.input_port_ip))
	# for recent version ryu - ofproto/nx_match.py
        # actions.append(parser.OFPActionSetField(tunnel_id_nxm=net_b_sgw_teid))
        # actions.append(parser.OFPActionSetField(tun_ipv4_dst=NET_B_SGW_IP))
        # actions.append(parser.OFPActionSetField(tun_ipv4_src=NET_B_PGW_IP))
        actions.append(parser.OFPActionOutput(pgwu.ENCAP))
        self.set_flow_mod(datapath, match, actions, priority)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        address = datapath.address
        ip = address[0]
        port = address[1]

        in_port = msg.match['in_port']
        self.logger.info("packet in_port = %s from [%s (%s)]", in_port, ip, port)
        #pkt = packet.Packet(msg.data)
        #eth = pkt.get_protocols(ethernet.ethernet)[0]
        #dst = eth.dst
        #src = eth.src
        #self.logger.info("packet in src = %s dst = %s in_port = %s from [%s (%s)]", src, dst, in_port, ip, port)
