import json

IP="ip"
MAC="mac"

class eNBs:
    def __init__(self, filename):
        with open(filename) as data_file:    
            data = json.load(data_file)

        self.enb_info = {}
        for (key,val) in data.iteritems():
            self.enb_info[key] = val;

    def get_enb_info(self,enb_name):
        enb = self.enb_info[enb_name]
        return (enb[IP], enb[MAC])

enb = eNBs("enb.json")
enb_info = enb.get_enb_info("enodeb07")
print enb_info[0]
print enb_info[1]
