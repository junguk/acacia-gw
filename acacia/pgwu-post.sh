#!/bin/bash

# Start up a PGWU instance
SUDO="/usr/bin/sudo"
OEPCBIN="/opt/OpenEPC/phantomnet"

$SUDO $OEPCBIN/run_wharf.pl pgwu \
    || { echo "Could not start PGWU wharf process!" && exit 1; }

echo "PGWU wharf process started."
exit 0;
