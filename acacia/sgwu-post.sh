#!/bin/bash

# Start up a SGWU instance
SUDO="/usr/bin/sudo"
OEPCBIN="/opt/OpenEPC/phantomnet"

$SUDO $OEPCBIN/run_wharf.pl sgwu \
    || { echo "Could not start SGWU wharf process!" && exit 1; }

echo "SGWU wharf process started."
exit 0;
