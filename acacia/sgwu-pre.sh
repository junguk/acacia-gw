#!/bin/bash

# Start up a second SGWU instance

SGWU_CUSTOM_CONF="/tmp/sgwu.xml"
SGWU_ORIG_CUSTOM_CONF="/opt/acacia-gw/acacia/xml/sgwu_orig.xml"

cp $SGWU_ORIG_CUSTOM_CONF $SGWU_CUSTOM_CONF
get_interface="/opt/acacia-gw/acacia/get_interface_map.pl"
mgmt=$($get_interface | grep -w mgmt | awk '{print $3}')
net_d=$($get_interface | grep -w net-d[[:blank:]] | awk '{print $3}')
net_b=$($get_interface | egrep "net-b|net_b" | awk '{print $3}')


echo $mgmt
echo $net_d
echo $net_b

sudo ip link set $mgmt down
sudo ip link set $net_d down
sudo ip link set $net_b down

sudo ip link set $mgmt up
sudo ip link set $net_d up
sudo ip link set $net_b up

sed -i "s/net_d/$net_d/" $SGWU_CUSTOM_CONF
sed -i "s/net_b/$net_b/" $SGWU_CUSTOM_CONF


SUDO="/usr/bin/sudo"
OEPCETC="/opt/OpenEPC/etc"
OEPCBIN="/opt/OpenEPC/phantomnet"

SGWU_CONF_PATH="sgwu.xml"

if [ ! -e $SGWU_CUSTOM_CONF ]
then
	echo "Error: Unable to find second SGWU config: $SGWU_CUSTOM_CONF"
	exit 1;
fi

$SUDO $OEPCBIN/check_oepc_config.pl -i $SGWU_CUSTOM_CONF $SGWU_CONF_PATH \
    || { echo "Could not install secondary SGWU config file!" && exit 1; }


echo "SGWU xml was placed in $OEPCETC"
exit 0;
