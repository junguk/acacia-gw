#!/bin/bash

# Start up a second PGWU instance


get_interface="/opt/acacia-gw/acacia/get_interface_map.pl"
net_a=$($get_interface | egrep "net-a|net_a" | awk '{print $3}')


sudo ip route del 192.168.3.0/24 via 192.168.1.10 dev $net_a
sudo ip route add 192.168.3.0/24 via 192.168.1.210 dev $net_a


exit 0;
