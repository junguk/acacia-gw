#!/bin/bash

# Start up a SGWC instance
SUDO="/usr/bin/sudo"
OEPCBIN="/opt/OpenEPC/phantomnet"

PGWC_CUSTOM_CONF="/tmp/pgwc.xml"
PGWC_ORIG_CUSTOM_CONF="/opt/acacia-gw/acacia/xml/pgwc_orig.xml"
PGWC_CONF_PATH="pgwc.xml"

# To get PGWU-IP
PGWU_MGMT="192.168.254.210"
PGWU_NET_A="192.168.1.210"
while [ true ]
do
# To avoid "Are you sure you want to continue connecting (yes/no)? yes"
ip=$(ssh "-o StrictHostKeyChecking no" 192.168.254.210 ip addr | grep 192.168.1.210)
net_a=$(echo $ip | awk '{print $7}')
echo $net_a

if [ ! -z $net_a ]
then
        echo "Get physical interface name for net_a in pgwu"
        echo $net_a
        break;
else
        echo "Cannot get physical interface name for net_a in pgwu"
fi

sleep 1
done

# replace net_a with "physical interface name" in pgwu
cp $PGWC_ORIG_CUSTOM_CONF $PGWC_CUSTOM_CONF
sed -i "s/net_a/$net_a/" $PGWC_CUSTOM_CONF

$SUDO $OEPCBIN/check_oepc_config.pl -i $PGWC_CUSTOM_CONF $PGWC_CONF_PATH \
    || { echo "Could not install PGWC config file!" && exit 1; }


$SUDO $OEPCBIN/kill_wharf.pl pgwc \
    || { echo "Could not kill PGWC wharf process!" && exit 1; }

$SUDO $OEPCBIN/run_wharf.pl pgwc \
    || { echo "Could not start PGWC wharf process!" && exit 1; }


echo "PGWC wharf process started."
exit 0;
