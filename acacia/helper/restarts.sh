#!/bin/bash

function restart_mme
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo /opt/OpenEPC/bin/mme.stop.sh"
	ssh $user@$IP "sudo /opt/OpenEPC/bin/mme.start.sh"
}


function restart_sgwc
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo /opt/OpenEPC/bin/sgwc.stop.sh"
	ssh $user@$IP "sudo /opt/OpenEPC/bin/sgwc.start.sh"
}


function restart_pgwc
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo /opt/OpenEPC/bin/pgwc.stop.sh"
	ssh $user@$IP "sudo /opt/OpenEPC/bin/pgwc.start.sh"
}

function stop_pgwu
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo /opt/OpenEPC/bin/pgwu.kill.sh"
}

function stop_sgwu
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo /opt/OpenEPC/bin/sgwu.kill.sh"
}

function start_pgwu
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo /opt/OpenEPC/bin/pgwu.start.sh"
}


function start_sgwu
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo /opt/OpenEPC/bin/sgwu.start.sh"
}

function restart_epc_enabler
{
	IP=$1
	user=$2
	ssh $user@$IP "sudo  /opt/OpenEPC/bin/squid_rx_client.kill.sh"
     	ssh $user@$IP "sudo  /opt/OpenEPC/bin/squid_rx_client.start.sh"

	ssh $user@$IP "sudo  /opt/OpenEPC/bin/hss.stop.sh"
	ssh $user@$IP "sudo  /opt/OpenEPC/bin/hss.start.sh"

	ssh $user@$IP "sudo  /opt/OpenEPC/bin/aaa.stop.sh"
	ssh $user@$IP "sudo  /opt/OpenEPC/bin/aaa.start.sh"

	ssh $user@$IP "sudo  /opt/OpenEPC/bin/cdf.stop.sh"
	ssh $user@$IP "sudo  /opt/OpenEPC/bin/cdf.start.sh"

	ssh $user@$IP "sudo  /opt/OpenEPC/bin/pcrf.stop.sh"
	ssh $user@$IP "sudo  /opt/OpenEPC/bin/pcrf.start.sh"
}

user=$(whoami)
echo "mme.flush" | nc 192.168.254.80 10000
restart_mme 192.168.254.80 $user

stop_pgwu 192.168.254.210 $user
stop_sgwu 192.168.254.220 $user

echo "gw_bindings.flush" | nc 192.168.254.20 10000
echo "gw_bindings.flush" | nc 192.168.254.19 10000

restart_pgwc 192.168.254.20 $user
restart_sgwc 192.168.254.20 $user

start_pgwu 192.168.254.210 $user
start_sgwu 192.168.254.220 $user

restart_epc_enabler 192.168.254.30 $user
