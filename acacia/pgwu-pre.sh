#!/bin/bash

# Start up a second PGWU instance

PGWU_ORIG_CUSTOM_CONF="/opt/acacia-gw/acacia/xml/pgwu_orig.xml"
PGWU_CUSTOM_CONF="/tmp/pgwu.xml"

cp $PGWU_ORIG_CUSTOM_CONF $PGWU_CUSTOM_CONF
get_interface="/opt/acacia-gw/acacia/get_interface_map.pl"
mgmt=$($get_interface | grep -w mgmt | awk '{print $3}')
net_a=$($get_interface | egrep "net-a|net_a" | awk '{print $3}')
net_b=$($get_interface | egrep "net-b|net_b" | awk '{print $3}')


echo $mgmt
echo $net_a
echo $net_b

sudo ip link set $mgmt down
sudo ip link set $net_a down
sudo ip link set $net_b down


sudo ip link set $mgmt up
sudo ip link set $net_a up
sudo ip link set $net_b up


sed -i "s/net_a/$net_a/" $PGWU_CUSTOM_CONF
sed -i "s/net_b/$net_b/" $PGWU_CUSTOM_CONF

SUDO="/usr/bin/sudo"
OEPCETC="/opt/OpenEPC/etc"
OEPCBIN="/opt/OpenEPC/phantomnet"

PGWU_CONF_PATH="pgwu.xml"

if [ ! -e $PGWU_CUSTOM_CONF ]
then
	echo "Error: Unable to find second PGWU config: $PGWU_CUSTOM_CONF"
	exit 1;
fi

$SUDO $OEPCBIN/check_oepc_config.pl -i $PGWU_CUSTOM_CONF $PGWU_CONF_PATH \
    || { echo "Could not install secondary PGWU config file!" && exit 1; }

sudo ip route del default
sudo ip route add default via 192.168.1.41 dev $net_a
sudo ip route add 192.168.0.0/16 via 192.168.2.220 dev $net_b

echo "PGWU xml was placed in $OEPCETC"
exit 0;
