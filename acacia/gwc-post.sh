#!/bin/bash

# Start up a SGWC instance
SUDO="/usr/bin/sudo"
OEPCBIN="/opt/OpenEPC/phantomnet"

$SUDO $OEPCBIN/run_wharf.pl sgwc \
    || { echo "Could not start SGWC wharf process!" && exit 1; }

echo "SGWC wharf process started."


$SUDO $OEPCBIN/run_wharf.pl pgwc \
    || { echo "Could not start SGWC wharf process!" && exit 1; }

echo "PGWC wharf process started."
exit 0;
