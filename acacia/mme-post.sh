#!/bin/bash

# Start up a MME instance
SUDO="/usr/bin/sudo"
OEPCBIN="/opt/OpenEPC/phantomnet"

$SUDO $OEPCBIN/run_wharf.pl mme \
    || { echo "Could not start secondary MME wharf process!" && exit 1; }

echo "MME wharf process started."
