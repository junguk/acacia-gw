#!/bin/bash

# Start up a second SGWC instance

SGWC_CUSTOM_CONF="/opt/acacia-gw/acacia/xml/sgwc.xml"



SUDO="/usr/bin/sudo"
OEPCETC="/opt/OpenEPC/etc"
OEPCBIN="/opt/OpenEPC/phantomnet"

SGWC_CONF_PATH="sgwc.xml"
get_interface="/opt/acacia-gw/acacia/get_interface_map.pl"
mgmt=$($get_interface | grep -w mgmt | awk '{print $3}')
sudo ip addr add 192.168.254.19/24 dev $mgmt
sudo ip addr add 192.168.254.10/24 dev $mgmt

if [ ! -e $SGWC_CUSTOM_CONF ]
then
	echo "Error: Unable to find SGWC config: $SGWC_CUSTOM_CONF"
	exit 1;
fi

$SUDO $OEPCBIN/check_oepc_config.pl -i $SGWC_CUSTOM_CONF $SGWC_CONF_PATH \
    || { echo "Could not install SGWC config file!" && exit 1; }


#if [ ! -e $PGWC_ORIG_CUSTOM_CONF ]
#then
#	echo "Error: Unable to find second PGWC config: $PGWC_ORIG_CUSTOM_CONF"
#	exit 1;
#fi


## To get PGWU-IP
#PGWC_CUSTOM_CONF="/tmp/pgwc.xml"
#PGWC_ORIG_CUSTOM_CONF="/opt/acacia-gw/acacia/xml/pgwc_orig.xml"
#PGWU_MGMT="192.168.254.210"
#PGWU_NET_A="192.168.1.210"
#while [ true ]
#do
## To avoid "Are you sure you want to continue connecting (yes/no)? yes"
#ip=$(ssh -o "StrictHostKeyChecking no" $PGWU_MGMT "ip addr | grep $PGWU_NET_A")
#net_a=$(echo $ip | awk '{print $7}')
#
#if [ ! -z $net_a ]
#then
#        echo "Get physical interface name for net_a in pgwu"
#        echo $net_a
#        break;
#else
#        echo "Cannot get physical interface name for net_a in pgwu"
#fi
#
#sleep 1
#done
#
## replace net_a with "physical interface name" in pgwu
#cp $PGWC_ORIG_CUSTOM_CONF $PGWC_CUSTOM_CONF
#sed -i "s/net_a/$net_a/" $PGWC_CUSTOM_CONF

#$SUDO $OEPCBIN/check_oepc_config.pl -i $PGWC_CUSTOM_CONF $PGWC_CONF_PATH \
#    || { echo "Could not install secondary PGWC config file!" && exit 1; }

#echo "GWCs xmls were placed in $OEPCETC"
exit 0;
