#!/bin/bash


# set up ip address for ntp server


SUDO="/usr/bin/sudo"
OEPCETC="/opt/OpenEPC/etc"
OEPCBIN="/opt/OpenEPC/phantomnet"
MMEOEPCBIN="/opt/OpenEPC/bin"


MME_CUSTOM_CONF="/opt/acacia-gw/acacia/xml/mme.xml"
MME_CONF_PATH="mme.xml"

$SUDO $OEPCBIN/check_oepc_config.pl -i $MME_CUSTOM_CONF $MME_CONF_PATH \
    || { echo "Could not install secondary SGWC config file!" && exit 1; }

if [ ! -e $MME_CUSTOM_CONF ]
then
        echo "Error: Unable to find second MME config: $MME_CUSTOM_CONF"
        exit 1;
fi


get_interface="/opt/acacia-gw/acacia/get_interface_map.pl"
net_d=$($get_interface | grep -w net-d[[:blank:]] | awk '{print $3}')
sudo ip addr add 192.168.4.20/24 dev $net_d

# For ntp server
sudo ip addr add 192.168.1.254/24 dev $net_d

$SUDO $MMEOEPCBIN/start_ipa_dhcpd.pl || { echo "Could not run dhcpd!" && exit 1; }
echo "Run dhcp server."

$SUDO $MMEOEPCBIN/start_ipa_ntpd.pl  || { echo "Could not run ntpd!" && exit 1; }
echo "Run ntp server."
