#!/usr/bin/env python

"""
An experiment profile for ACACIA Mobile Edge Network
"""

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.urn as URN

#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    EPCIMG = PN.PNDEFS.DEF_BINOEPC_IMG
    UBUNTU14_IMG = URN.Image(PN.PNDEFS.PNET_AM, "emulab-ops:UBUNTU14-64-STD")
    ACACIA_EPC = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ACACIA-BINOEPC-IMG")
    UE_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")
    ACACIA_LOCAL_GWU = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ACACIA-LOCAL-GWU")
    #SGWU_START = "/opt/acacia-gw/sgwu/run_sgwu.sh"
    #PGWU_START = "/opt/acacia-gw/pgwu/run_pgwu.sh"
    ENODEB_HWTYPE = "enodeb"
    UE_HWTYPE = "nexus5"
#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()
# disable the static routes from Emulab/PhantomNet
# request.SetRoutingStyle("none")


# Define some parameters for OpenEPC experiments. 
pc.defineParameter("HWTYPE","Node Hardware Type",
                   portal.ParameterType.STRING, "pc",
                   [("pc","Any available (compatible) physical machine type"),
                    ("pc3000","Emulab pc3000 nodes"),
                    ("d710","Emulab d710 nodes"),
                    ("d430","Emulab d430 nodes")],
                   longDescription="Specify which node resource type to use for OpenEPC nodes. Note that only those types that are compatible with the OpenEPC image(s) are listed.")

pc.defineParameter("NUMCLI", "Number of emulated clients (UEs)",
                   portal.ParameterType.INTEGER, 0,
                   longDescription="Specify the number of emulated client (User Equipment) resources to allocate. This number must be between 0 and 32 currently.",
                   advanced=True)

pc.defineParameter("NUMENB", "Number of emulated eNodeB nodes.",
                   portal.ParameterType.INTEGER, 0,
                   [0,1,2,3],
                   longDescription="Number of emulated eNodeB (LTE base station) nodes to allocate.  May be from 0 to 3 (inclusive).",
                   advanced=True)

pc.defineParameter("LINKBW","Default Link Bandwidth (Mbps)",
                   portal.ParameterType.INTEGER, 0,
                   longDescription="Specify the default LAN bandwidth in Mbps for all EPC LANs. Leave at \"0\" to indicate \"best effort\". Values that do not line up with common physical interface speeds (e.g. 10, 100, or 1000) WILL cause the insertion of link shaping elements.",
                   advanced=True)

pc.defineParameter("FIXEDUE", "Bind to a specific UE",
                   portal.ParameterType.STRING, "",
                   longDescription="Input the name of a PhantomNet UE node to allocate (e.g., \'ue1\').  Leave blank to let the mapping algorithm choose.",
                    advanced=True)
pc.defineParameter("FIXEDENB", "Bind to a specific eNodeB",
                   portal.ParameterType.STRING, "",
                   longDescription="Input the name of a PhantomNet eNodeB device to allocate (e.g., \'enodeb01\').  Leave blank to let the mapping algorithm choose.  If you bind both UE and eNodeB devices, mapping will fail unless there is path between them via the attenuator matrix.",
                    advanced=True)

#
# Get any input parameter values that will override our defaults.
#
params = pc.bindParameters()

#
# Verify parameters and throw errors.
#
if params.NUMCLI > 32 or params.NUMCLI < 0:
    perr = portal.ParameterError("You can only have 32 or fewer client nodes!", ['NUMCLI'])
    pc.reportError(perr)

if params.NUMENB < 0 or params.NUMENB > 3:
    perr = portal.ParameterError("You can have no more than three eNodeB nodes!", ['NUMENB'])
    pc.reportError(perr)

if (params.NUMENB > 0 and params.NUMCLI == 0) or \
   (params.NUMCLI > 0 and params.NUMENB == 0):
    perr = portal.ParamaterError("You must request at least one emulated UE and one emulated eNodeB or zero of both!")
    pc.reportError(perr)

if int(params.LINKBW) not in [0, 10, 100, 1000]:
    pwarn = portal.ParameterWarning("You are asking for a default link bandwidth that is NOT a standard physical link speed. Link shaping resources WILL be inserted!", ['LINKBW'])
    pc.reportWarning(pwarn)

# FIXME: put in check for fixed eNB and UE device names.

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Scale link bandwidth parameter to kbps
#
params.LINKBW *= 1000

#
# Set the hardware and image for the epc node factory function
#
PN.EPCNodeFactorySettings.hardware_type = params.HWTYPE
PN.EPCNodeFactorySettings.do_sync_start = True


# Create the lans we need
mgmt = request.EPClan(PN.EPCLANS.MGMT)
net_a = request.EPClan(PN.EPCLANS.NET_A)
net_a.bandwidth = params.LINKBW

net_b_default = request.EPClan("net_b_default")
net_b_default.bandwidth = params.LINKBW
net_b_dedicated = request.EPClan("net_b_dedicated")
net_b_dedicated.bandwidth = params.LINKBW

net_d = request.EPClan(PN.EPCLANS.NET_D)
net_d.bandwidth = params.LINKBW

offload = request.EPClan("offload")
offload.bandwidth = params.LINKBW

# lan for a SDN controller
net_of = request.EPClan("net_of")
net_of.bandwidth = params.LINKBW


if params.NUMCLI > 0:
    an_lte = request.EPClan(PN.EPCLANS.AN_LTE)
    an_lte.bandwidth = params.LINKBW

# Add the core EPC nodes

# epc-enablers node
prehook = "/opt/acacia-gw/acacia/epc-pre.sh"
epcen = PN.mkepcnode("epc", PN.EPCROLES.ENABLERS, prehook=prehook, request = request)
epcen.disk_image = GLOBALS.ACACIA_EPC 
mgmt.addMember(epcen)
net_a.addMember(epcen)

# pgwu
ename = "pgwu"
prehook = "/opt/acacia-gw/acacia/pgwu-pre.sh"
posthook = "/opt/acacia-gw/acacia/pgwu-post.sh"
pgwu = PN.mkepcnode(ename, PN.EPCROLES.ANY, hname = ename, prehook=prehook, posthook=posthook, request = request)
pgwu.disk_image = GLOBALS.ACACIA_EPC 
cintf = mgmt.addMember(pgwu)
caddr = rspec.IPv4Address("192.168.254.210","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_a.addMember(pgwu)
caddr = rspec.IPv4Address("192.168.1.210","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_b_default.addMember(pgwu)
caddr = rspec.IPv4Address("192.168.2.210","255.255.255.0")
cintf.addAddress(caddr)

# PGWU1
ename = "pgwu1"
pgwu1 = request.RawPC(ename)
pgwu1.hardware_type = params.HWTYPE
pgwu1.disk_image = GLOBALS.ACACIA_LOCAL_GWU
cintf = net_of.addMember(pgwu1)
caddr = rspec.IPv4Address("192.168.20.211","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_b_dedicated.addMember(pgwu1)
caddr = rspec.IPv4Address("192.168.2.211","255.255.255.0")
cintf.addAddress(caddr)
cintf = offload.addMember(pgwu1)
caddr = rspec.IPv4Address("192.168.10.10","255.255.255.0")
cintf.addAddress(caddr)


# add offload server
ar = None
ar = request.RawPC("ar")
ar.hardware_type = params.HWTYPE
ar.disk_image = GLOBALS.UBUNTU14_IMG 
cintf = offload.addMember(ar)
caddr = rspec.IPv4Address("192.168.10.11","255.255.255.0")
cintf.addAddress(caddr)

# sgwu
ename = "sgwu"
prehook = "/opt/acacia-gw/acacia/sgwu-pre.sh"
posthook = "/opt/acacia-gw/acacia/sgwu-post.sh"
sgwu = PN.mkepcnode(ename, PN.EPCROLES.ANY, hname = ename, prehook=prehook, posthook=posthook, request = request)
sgwu.disk_image = GLOBALS.ACACIA_EPC 
cintf = mgmt.addMember(sgwu)
caddr = rspec.IPv4Address("192.168.254.220","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_d.addMember(sgwu)
caddr = rspec.IPv4Address("192.168.4.222","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_b_default.addMember(sgwu)
caddr = rspec.IPv4Address("192.168.2.220","255.255.255.0")
cintf.addAddress(caddr)


# sgwu1
ename = "sgwu1"
sgwu1 = request.RawPC(ename)
sgwu1.hardware_type = params.HWTYPE
sgwu1.disk_image = GLOBALS.ACACIA_LOCAL_GWU
cintf = net_of.addMember(sgwu1)
caddr = rspec.IPv4Address("192.168.20.221","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_d.addMember(sgwu1)
caddr = rspec.IPv4Address("192.168.4.221","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_b_dedicated.addMember(sgwu1)
caddr = rspec.IPv4Address("192.168.2.221","255.255.255.0")
cintf.addAddress(caddr)



# mme
ename = "mme"
prehook = "/opt/acacia-gw/acacia/mme-pre.sh"
posthook = "/opt/acacia-gw/acacia/mme-post.sh"
mme = PN.mkepcnode(ename, PN.EPCROLES.ANY, hname = ename, prehook=prehook, posthook=posthook, request = request)
mme.disk_image = GLOBALS.ACACIA_EPC
cintf = mgmt.addMember(mme)
caddr = rspec.IPv4Address("192.168.254.80","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_d.addMember(mme)
caddr = rspec.IPv4Address("192.168.4.80","255.255.255.0")
cintf.addAddress(caddr)


# sgwc-pgwc
ename = "sgwc-pgwc"
prehook = "/opt/acacia-gw/acacia/gwc-pre.sh"
posthook = "/opt/acacia-gw/acacia/gwc-post.sh"
sgwc_pgwc = PN.mkepcnode(ename, PN.EPCROLES.ANY, hname = ename, prehook=prehook, posthook=posthook, request = request)
sgwc_pgwc.disk_image = GLOBALS.ACACIA_EPC
cintf = mgmt.addMember(sgwc_pgwc)
caddr = rspec.IPv4Address("192.168.254.20","255.255.255.0")
cintf.addAddress(caddr)
cintf = net_of.addMember(sgwc_pgwc)
caddr = rspec.IPv4Address("192.168.20.20","255.255.255.0")
cintf.addAddress(caddr)


# Add a node to act as the ADB target host
adb_t = request.RawPC("adb-tgt")
adb_t.disk_image = GLOBALS.ADB_IMG

# Add a real eNodeB
renb1 = request.eNodeB("renb1")
if params.FIXEDENB:
    renb1.component_id = params.FIXEDENB
renb1.hardware_type = GLOBALS.ENODEB_HWTYPE
net_d.addMember(renb1)
net_d.bandwidth = 1000 * 1000 # Hack: must set bw on net_d to 100Mbps.
renb1_rflink1 = renb1.addInterface("renb1_rflink1")

# Add a real UE
rue1 = request.UE("rue1")
if params.FIXEDUE:
    rue1.component_id = params.FIXEDUE
rue1.hardware_type = GLOBALS.UE_HWTYPE
rue1.disk_image = GLOBALS.UE_IMG
rue1.adb_target = "adb-tgt"
rue1_rflink1 = rue1.addInterface("rue1_rflink1")

# Create the RF link between the real UE and eNodeB
rflink1 = request.RFLink("rflink1")
rflink1.addInterface(rue1_rflink1)
rflink1.addInterface(renb1_rflink1)

#
# Create the requested number of eNodeB nodes
#
for i in range(1, params.NUMENB + 1):
    ename = "enb%d" % i
    enb = PN.mkepcnode(ename, PN.EPCROLES.ENODEB, hname = ename, request = request)
    mgmt.addMember(enb)
    net_d.addMember(enb)
    an_lte.addMember(enb)

#
# Now pop in the requested number of emulated clients (UEs).
#
for i in range(1, params.NUMCLI + 1):
    cname = "client%d" % i
    client = PN.mkepcnode(cname, PN.EPCROLES.CLIENT, hname = cname, request = request)
    mgmt.addMember(client)
    an_lte.addMember(client)

#
# Print and go!
#
pc.printRequestRSpec(request)
