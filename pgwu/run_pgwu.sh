#!/bin/bash

ovs-vsctl del-br br0
kill `cd /usr/local/var/run/openvswitch && cat ovsdb-server.pid ovs-vswitchd.pid`
modprobe -r openvswitch

modprobe openvswitch
lsmod | grep openvswitch

mv /usr/local/etc/openvswitch/conf.db  /usr/local/etc/openvswitch/conf.db.bak
ovsdb-tool create /usr/local/etc/openvswitch/conf.db /usr/local/share/openvswitch/vswitch.ovsschema

################ Startup ###########################
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock \
		      --remote=db:Open_vSwitch,Open_vSwitch,manager_options \
		      --private-key=db:Open_vSwitch,SSL,private_key \
		      --certificate=db:Open_vSwitch,SSL,certificate \
		      --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert \
		      --pidfile --detach

		      
ovs-vsctl --no-wait init
ovs-vswitchd --pidfile --detach
ovs-appctl vlog/set info

get_interface="perl /opt/acacia-gw/get_interface_map.pl"
offload=$($get_interface | grep -w offload[[:blank:]] | awk '{print $3}')
net_b=$($get_interface | grep -w net_b_dedicated | awk '{print $3}')
net_of=$($get_interface | grep -w net_of | awk '{print $3}')


#Delete old flows on the bridge's interfaces.
#
echo -e "LAN\t\tIFACE\tPORT-NUMBER "
echo -e "offload\t\t$offload\t$offload_port"
echo -e "net_b\t$net_b\t$net_b_port"
echo -e "net_of\t\t$offload\t$offload_port"

ifconfig $net_b down
ifconfig $offload down
ifconfig $net_of down


ifconfig $net_b up
ifconfig $offload up
ifconfig $net_of up

ovs-vsctl add-br br0

#ovs-vsctl add-br br0 -- set port br0 mac=\"00:04:23:b7:12:da\"
ovs-vsctl set Open_vSwitch . other_config:datapath_id=0
ovs-vsctl set-fail-mode br0 secure


# TODO : CAN GET FROM INITIAL SCRIPT

#net_b=eth3
#offload=eth5

offload_ip="192.168.10.10"
pgw_b_ip="192.168.2.211"
net_of_ip="192.168.20.211"
net_of_ctl_ip="192.168.20.221"
ifconfig $offload 0.0.0.0
ifconfig $net_b 0.0.0.0
ifconfig br0 $offload_ip
ifconfig br0:1 $pgw_b_ip

#sudo ethtool -K $net_b gro off
#sudo ethtool -K $net_b gso off
#sudo ethtool -K $net_b tso off
sudo ethtool -K $offload gro off
sudo ethtool -K $offload gso off
sudo ethtool -K $offload tso off


ovs-vsctl add-port br0 $net_b -- set interface $net_b ofport_request=1
ovs-vsctl add-port br0 $offload -- set interface $offload ofport_request=2
ovs-vsctl add-port br0 gtp1 -- set interface gtp1 type=gtp options:remote_ip=flow options:in_key=flow options:dst_port=2152 ofport_request=3
ovs-vsctl add-port br0 gtp2 -- set interface gtp2 type=gtp options:remote_ip=flow options:in_key=flow options:dst_port=2153 ofport_request=4
ovs-vsctl add-port br0 gtp3 -- set interface gtp3 type=gtp options:remote_ip=flow options:local_ip=flow options:in_key=flow options:out_key=flow options:dst_port=2154 ofport_request=5
ovs-vsctl set bridge br0 protocols=OpenFlow10,OpenFlow12,OpenFlow13

sudo ovs-ofctl dump-ports-desc br0
ovs-vsctl show

## set controller
ovs-vsctl set bridge br0 other-config:disable-in-band=true
ovs-vsctl set controller br0 connection-mode=out-of-band
ovs-vsctl get-controller br0
ovs-vsctl set-controller br0 tcp:$net_of_ctl_ip
